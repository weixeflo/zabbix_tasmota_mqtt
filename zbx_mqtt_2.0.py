#!/usr/bin/python3
import json
import paho.mqtt.client as mqtt
from pyzabbix import ZabbixMetric, ZabbixSender
import argparse
import pprint
import os

parser = argparse.ArgumentParser(description='Interface to query a MQTT-topic and provide it so that is is parseable for Zabbix')

parser.add_argument('-s', '--server', help='IP/DNS of the MQTT-Server', default="127.0.0.1")
parser.add_argument('-p', '--port', help='Port of the MQTT-Service', type=int, default=1883)
parser.add_argument('-t', '--topic', help='MQTT-Topic (e.g. tele/#)', required=True)

args = parser.parse_args()
print(args.server)
print(args.port)

agent_config_path="/usr/local/etc/zabbix_agentd.conf"


# check if agent-config is present
if os.path.exists(agent_config_path):
	print("Agent-config present")
else:
	print("Agetn config not found")
	exit(1)


def on_connect(client, userdata, flags, rc):
	#print("Connected with result code "+str(rc))
	client.subscribe(args.topic)

def on_message(client, userdata, msg):
	pprint.pprint(msg.topic)
	payload=msg.payload.decode()
	print("Payload: "+payload)
	device=str(msg.topic.split("/")[1])
	topic_id=str(msg.topic.split("/")[2])
	print(device)
	print(topic_id)
	print("*************************************************"+chr(10))
	packet = [
		ZabbixMetric(device, 'json_sensor_data_'+topic_id, payload )
	]
	result = ZabbixSender(use_config=agent_config_path).send(packet)
	print(result)
	print('#######################################################\n\n')

	
	

client = mqtt.Client()
client.connect(args.server, args.port, 60)
client.on_connect = on_connect
client.on_message = on_message
client.loop_forever()
